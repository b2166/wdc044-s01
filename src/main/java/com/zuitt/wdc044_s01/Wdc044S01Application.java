package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Scanner;

@SpringBootApplication // Annotations
@RestController
public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {

		return String.format("Hello %s!", name);

	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "user") String name) {

		return String.format("hi %s!", name);

	}

	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "user") String name, @RequestParam(value = "age", defaultValue = "undefined") String age) {

		return String.format("Hello %s! Your age is %s.", name, age);

	}

}
